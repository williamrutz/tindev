import React from 'react';
import './Main.css';

import logo from '../assets/Tinder.svg';
import deslike from '../assets/dislike.png';
import like from '../assets/like.png';

export default function Main({ match }) {
    return(
        <div className="main-container">
            <form>
                <img className="logo" src={logo} alt="Tindev" />
                 <ul>
                     <li>
                         <img src="https://avatars0.githubusercontent.com/u/4248081?v=4" alt=""/>
                         <footer>
                             <strong>William</strong>
                             <p>testee</p>
                         </footer>
                         <div className="buttons">
                            <button type="button">
                                <img src={deslike} alt="Deslike"/>
                            </button>
                            <button type="button">
                                <img src={like} alt="Like"/>
                            </button>
                         </div>
                     </li>
                     <li>
                         <img src="https://avatars0.githubusercontent.com/u/4248081?v=4" alt=""/>
                         <footer>
                             <strong>William</strong>
                             <p>testee</p>
                         </footer>
                         <div className="buttons">
                            <button type="button">
                                <img src={deslike} alt="Deslike"/>
                            </button>
                            <button type="button">
                                <img src={like} alt="Like"/>
                            </button>
                         </div>
                     </li>
                     <li>
                         <img src="https://avatars0.githubusercontent.com/u/4248081?v=4" alt=""/>
                         <footer>
                             <strong>William</strong>
                             <p>testee</p>
                         </footer>
                         <div className="buttons">
                            <button type="button">
                                <img src={deslike} alt="Deslike"/>
                            </button>
                            <button type="button">
                                <img src={like} alt="Like"/>
                            </button>
                         </div>
                     </li>
                     <li>
                         <img src="https://avatars0.githubusercontent.com/u/4248081?v=4" alt=""/>
                         <footer>
                             <strong>William</strong>
                             <p>testee</p>
                         </footer>
                         <div className="buttons">
                            <button type="button">
                                <img src={deslike} alt="Deslike"/>
                            </button>
                            <button type="button">
                                <img src={like} alt="Like"/>
                            </button>
                         </div>
                     </li>
                 </ul>
            </form>
        </div>
    )
}