const express = require('express');
const DevController = require('./controllers/DevController.js');
const LikeController = require('./controllers/LikeController.js');
const DeslikeController = require('./controllers/DeslikeController.js');

const routes = express.Router();

routes.post('/devs', DevController.store);
routes.get('/devs', DevController.index);
routes.post('/devs/:devId/likes', LikeController.store);
routes.post('/devs/:devId/deslikes', DeslikeController.store);

module.exports = routes;